import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { HttpClient } from '@angular/common/http';
import { ReservationService } from '../Services/Reservation.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-baber-page',
  templateUrl: './baber-page.component.html',
  styleUrls: ['./baber-page.component.css']
})
export class BaberPageComponent implements OnInit {

  constructor(private rout:Router,private http:HttpClient,private service:ReservationService) { }
  displayedColumns: string[] = ['OrderDate', 'OrderHour', 'OrderedBy', 'PhoneNumber'];
  dataSource=new MatTableDataSource(ELEMENT_DATA);
  resss=ELEMENT_DATA;
  ngOnInit() {
   
    if(this.service.role=="C900A93C-448C-4C21-8D7F-2FAABDFBC023"){
      this.service.OrderID=" ";
      this.service.userName=" ";
      let urladdrss='/Login';
      this.rout.navigateByUrl(urladdrss);
    }
    this.http.post<PeriodicElement[]>('http://localhost:60037/api/users/BaberSeeORders?BarberID='+this.service.barberID,'a').pipe(map(ress=>{
      const getarr=[];
      for(const key in ress){
        getarr.push(ress[key]);
      }
      console.log(getarr);
    
    return getarr;})).subscribe(res=>{
      // for(const k in res){
      //   this.resss.push(res[k]);
      // }
      console.log(res);
      this.dataSource=new MatTableDataSource(res);
    })
  }
 

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
export interface PeriodicElement {
  OrderDate: string;
  OrderHour: number;
  OrderedBy: string;
  PhoneNumber: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
  {OrderDate: "1", PhoneNumber: 'Hydrogen', OrderHour: 1.0079, OrderedBy: 'H'},
];
