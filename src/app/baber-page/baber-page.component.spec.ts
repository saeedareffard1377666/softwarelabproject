import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaberPageComponent } from './baber-page.component';

describe('BaberPageComponent', () => {
  let component: BaberPageComponent;
  let fixture: ComponentFixture<BaberPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaberPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaberPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
