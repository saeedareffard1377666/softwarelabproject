import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderedFavListComponent } from './ordered-fav-list.component';

describe('OrderedFavListComponent', () => {
  let component: OrderedFavListComponent;
  let fixture: ComponentFixture<OrderedFavListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderedFavListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedFavListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
