import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private router: Router) {
    iconRegistry.addSvgIcon(
      'Barber-shop',
      sanitizer.bypassSecurityTrustResourceUrl('assets/baber-scissors.svg'));
    iconRegistry.addSvgIcon(
        'Menu',
        sanitizer.bypassSecurityTrustResourceUrl('assets/menu.svg'));
    iconRegistry.addSvgIcon(
          'reserved',
          sanitizer.bypassSecurityTrustResourceUrl('assets/reserved.svg'));
    }
    BarbershopName = 'شماره یک';
    public  LoadLoB() {
      this.router.navigate(['ListOfBShops']);
    }
  }



