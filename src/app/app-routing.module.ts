import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ListofBarberShopsComponent } from './listof-barber-shops/listof-barber-shops.component';
import { LoginComponent } from './login/login.component';
import { SetTimeForBarberShopComponent } from './listof-barber-shops/set-time-for-barber-shop/set-time-for-barber-shop.component';
import { OrderedFavListComponent } from './ordered-fav-list/ordered-fav-list.component';
import { BaberPageComponent } from './baber-page/baber-page.component';


const routes: Routes = [

  {path: 'ListOfBShops', component: ListofBarberShopsComponent},
  {path: 'Login', component: LoginComponent},
  {path: 'SetTheOrder/:ID', component: SetTimeForBarberShopComponent},
  {path:'Order',component:OrderedFavListComponent},
  {path:'Barber',component:BaberPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
