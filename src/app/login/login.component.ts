import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { ReservationService } from '../Services/Reservation.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { stringify } from 'querystring';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private rout:Router,private http : HttpClient,private service:ReservationService ) { }
  ReggB=false;//register for barber
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required, Validators.email]);
  inputusername='';
  inputpass='';
  result='';
  ngOnInit() {
    this.service.OrderID=" ";
      this.service.userName=" ";
    const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
  getErrorMessageForPassword() {
    return this.password.hasError('required') ? 'You must enter a value' :
        this.password.hasError('password') ? 'Not a valid password' :
            '';
  }
  Login(usr:String,pass:String){
    usr = (document.getElementById('usr') as HTMLInputElement).value;
    pass=(document.getElementById('passwd') as HTMLInputElement).value;
    console.log(this.inputusername);
    console.log(pass);
    var addrss = 'http://localhost:60037/api/users/Login?UN='+usr;
    console.log(addrss);
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*' });
  let options = { headers: headers };
  // this.http.post(addrss, JSON.stringify(pass),options).pipe(map(aws=>{
  //   const getarr=[];
  //   for(const k in aws){
  //     getarr.push(aws[k]);
  //   }
  //   return getarr;
  // })

  // ).subscribe(qwe=>{
  //   if(qwe[1]=="C900A93C-448C-4C21-8D7F-2FAABDFBC023"){
      
  //     this.service.role='C900A93C-448C-4C21-8D7F-2FAABDFBC023'
  //     let urladdrss = '/ListOfBShops';
  //     this.rout.navigateByUrl(urladdrss);
  //   }
  //   else{
  //     console.log(qwe);
  //     alert();
  //     let urladdrss='/Barber';
  //     this.service.barberID=qwe[1];
  //     this.rout.navigateByUrl(urladdrss);
  //   }
  // })
     this.http.post(addrss, JSON.stringify(pass),options).pipe(map(ress=>{
      const getarr=[];
      for(const key in ress){
        getarr.push(ress[key]);
      }
      console.log(getarr);
    
    return getarr;})).subscribe(adw=>{
      console.log(adw);
      let mm = adw.pop();
      console.log(mm["role"].toUpperCase());
      
      this.service.role=mm["role"].toUpperCase();
      this.service.userName=mm["answer"].toUpperCase();
      if(mm["role"].toUpperCase()==="C900A93C-448C-4C21-8D7F-2FAABDFBC023"){
        this.service.role='C900A93C-448C-4C21-8D7F-2FAABDFBC023'
        let urladdrss = '/ListOfBShops';
        this.rout.navigateByUrl(urladdrss);
      }
      else if(mm["answer"]==="Fail"){
        let urladdrss='/Login';
        alert("نام کاربری یا رمز عبور اشتباه است");
            // this.service.barberID=mm["role"];
            this.service.OrderID=" ";
            this.service.userName=" ";
           this.rout.navigateByUrl(urladdrss);
      }
      else{
        this.service.barberID=mm["role"];
        let urladdrss='/Barber';
        this.rout.navigateByUrl(urladdrss);
      }
      console.log(mm["answer"]);
    })
   
  }
  displayThem(){
    
    (document.getElementById("bbname") as HTMLInputElement).style.display="inline-block";
    (document.getElementById("bbaddress") as HTMLInputElement).style.display="inline-block";
    this.ReggB=true;
  }
  register(){
    let bbname;
    let bbaddress;
    let usrname=(document.getElementById("usrname")as HTMLInputElement).value;
    let fullname=(document.getElementById("fullname")as HTMLInputElement).value;
    let password=(document.getElementById("password")as HTMLInputElement).value;
    let usrphone=(document.getElementById("usrphone")as HTMLInputElement).value;
    let UserAddress=(document.getElementById("usraddress") as HTMLInputElement).value;
    if(this.ReggB===true){
       bbname=(document.getElementById("bbname")as HTMLInputElement).value;
       bbaddress=(document.getElementById("bbaddress")as HTMLInputElement).value;
    }
    else{
      bbname="asdasdasdad";
      bbaddress="ascsacsacsa";
    }
    
    let isbarber=this.ReggB;
    
    let objc={
      "User_ID":"usrID",
      "Username":usrname,
      "Password":password,
      "UserFullName":fullname,
      "UserAddress":UserAddress,
      "UserPhoneNumber":usrphone,
      "UserRole":"sc2"
    }
    this.http.post('http://localhost:60037/api/users/register/?IsBarber='+isbarber+'&barberName='+bbname+'&barberAddress='+bbaddress+'&barberPhoneNumber='+usrphone,objc)
    .subscribe(res=>{
      alert("کابر با موفقیت افزوده شد لطفا به قسمت لاگین بروید");
      const container = document.getElementById('container');
      container.classList.remove("right-panel-active");
    })
   
  }
}
