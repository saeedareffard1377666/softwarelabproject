import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Glob } from 'glob';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { element, Key } from 'protractor';
import {map} from 'rxjs/operators';
import { ReservationService } from '../Services/Reservation.service';

// @Component({
//   selector: 'app-listof-barber-shops',
//   templateUrl: './listof-barber-shops.component.html',
//   styleUrls: ['./listof-barber-shops.component.css']
// })
@Component({
  selector: 'app-listof-barber-shops',
  styleUrls: ['./listof-barber-shops.component.css'],
  templateUrl: './listof-barber-shops.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ListofBarberShopsComponent implements OnInit {
  // tslint:disable-next-line: no-use-before-declare
  dataSource=ELEMENT_DATA ;
  dt;
  columnsToDisplay = ['BarberID', 'Barbername', 'BarberScore'];
    expandedElement: PeriodicElement | null;
constructor(private r: Router,private http : HttpClient,private service:ReservationService) {

}
ngOnInit(){
  if(this.service.role!="C900A93C-448C-4C21-8D7F-2FAABDFBC023" || this.service.userName==""){
    this.service.OrderID=" ";
    this.service.userName=" ";
    let urladdrss='/Login';
    this.r.navigateByUrl(urladdrss);
  }
  this.http.get("http://localhost:60037/api/users/getAllBB")
  .pipe(map(ress=>{
    const getarr=[];
    for(const key in ress){
      getarr.push(ress[key]);
    }
    console.log(getarr);
  
  return getarr;}))
  .subscribe(
    res => {
      
      // this.dtg=res;
      for(const k in res){
        this.dataSource.push(res[k]);
      }
      this.dataSource=res;
      console.log(this.dataSource);  
     
    } 
  
  )

  
  //   ELEM :PeriodicElement;
  //  console.log(this.ELEM);
}
    Go(ID: string) {
      // console.log(ID);
     // tslint:disable-next-line: align
      // tslint:disable-next-line: prefer-const
      this.service.barberID=ID;
      let urladdrss = '/SetTheOrder/' + ID;
      this.r.navigateByUrl(urladdrss);
      }
    // GetAllbarberShops(){
    //   this.http.get("http://localhost:60037/api/users/getAllBB").subscribe(
    //     res => {
    //       this.dataSource=res;
    //       console.log(res);
    //     } 
    //   )
    // }
}


export interface PeriodicElement {

  BarberID: string;
  Barbername: string;
  BarberScore: number;
  BarberAddress: string;
  BarberPhoneNumber: string;
}



const ELEMENT_DATA: PeriodicElement[] = [
  {
    BarberID:"1",
    Barbername:'آرایشگاه یک',
    BarberScore:8,
    BarberAddress:'آدرس یک',
    BarberPhoneNumber:'09222222222'
  },  {
    BarberID:"1",
    Barbername:'آرایشگاه یک',
    BarberScore:8,
    BarberAddress:'آدرس یک',
    BarberPhoneNumber:'09222222222'
  }, {
    BarberID:"1",
    Barbername:'آرایشگاه یک',
    BarberScore:8,
    BarberAddress:'آدرس یک',
    BarberPhoneNumber:'09222222222'
  }
];
