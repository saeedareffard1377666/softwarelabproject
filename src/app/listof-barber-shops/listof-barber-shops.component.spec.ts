import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListofBarberShopsComponent } from './listof-barber-shops.component';

describe('ListofBarberShopsComponent', () => {
  let component: ListofBarberShopsComponent;
  let fixture: ComponentFixture<ListofBarberShopsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListofBarberShopsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListofBarberShopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
