import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetTimeForBarberShopComponent } from './set-time-for-barber-shop.component';

describe('SetTimeForBarberShopComponent', () => {
  let component: SetTimeForBarberShopComponent;
  let fixture: ComponentFixture<SetTimeForBarberShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetTimeForBarberShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetTimeForBarberShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
