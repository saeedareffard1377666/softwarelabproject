import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {DatePickerDirective, DatePickerComponent} from 'ng2-jalali-date-picker';
import * as moment from 'jalali-moment';
import { group } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import {map, find} from 'rxjs/operators';
import { MatButtonToggle } from '@angular/material/button-toggle';
import { MatRadioButton } from '@angular/material/radio';
import { ReservationService } from 'src/app/Services/Reservation.service';

@Component({
  selector: 'app-set-time-for-barber-shop',
  templateUrl: './set-time-for-barber-shop.component.html',
  styleUrls: ['./set-time-for-barber-shop.component.css']
})
export class SetTimeForBarberShopComponent implements OnInit {

  constructor(private routh: ActivatedRoute,private r: Router,private http :HttpClient,private service:ReservationService) { }
  myFlagForButtonToggle;
  number=[];
  @ViewChild('gr', {static: true}) gr;
  @ViewChild('DP', {static: true}) DP;
  @ViewChild('dayPicker',{static: true}) datePicker: DatePickerComponent;
  myFlagForSlideToggle  = true;
  TheElement: string;
  TheResultOfORder: string ;
  dateObject = '';
  IsDateOK = true;
  IsHour = true;
  result;
 
  ngOnInit() {
    if(this.service.role!="C900A93C-448C-4C21-8D7F-2FAABDFBC023" || this.service.userName==""){
      this.service.OrderID=" ";
      this.service.userName=" ";
      let urladdrss='/Login';
      this.r.navigateByUrl(urladdrss);
    }
   this.TheElement = this.routh.snapshot.params.ID;
   console.log(this.TheElement);
    
    //  for(const key in this.number){
    //    console.log(this.number[key]);
    //    document.getElementById(this.number[key]).style.backgroundColor="#f6060a";
    //   //  document.getElementById(this.number[key]).disabled=true;
    //  }
  }
  
  getBusytime(event:any){
    let list = [8,9,10,11,12,16,17,18,19,20,21];
    for(const i in list){
      document.getElementById(list[i].toString()).style.backgroundColor="white";
    }
    console.log();
    this.http.get<number[]>("http://localhost:60037/api/users/GetBusyTime?barberID="+this.TheElement+"&oderDate="+this.dateObject).subscribe(
      result =>{        
        this.number=result;
        console.log(this.number);
        this.number.forEach(element => {
        console.log(element);
         
        document.getElementById(element).style.backgroundColor="#f6060a";
       });
      }
    )
    
  }
  // ngOnChange(){
  //   console.log("k;ajdsv");
  //   this.http.get<number[]>("http://localhost:60037/api/users/GetBusyTime?barberID="+this.TheElement+"&oderDate="+this.dateObject).subscribe(
  //     result =>{
        
  //       this.number=result;
  //       console.log(this.number);
  //       this.number.forEach(element => {
  //        console.log(element);
  //       document.getElementById(element).style.backgroundColor="#f6060a";
  //      });
  //     })
  // }
  SetParaGraph() {
    let r=this.gr.value.substring(0,2);
    if(r.substring(0,1)=='0'){
    r=r.replace(r.substring(0,1),'');}
    console.log(r);
    
    
  let  order={
    //this.service.userName
    "orderBy":this.service.userName,
    "orderhour":r,
    "OrderDate":this.dateObject,
    "barbershopID":this.service.barberID
    //barberID
    }
    this.http.post('http://localhost:60037/api/users/OrderRequest',order).subscribe(
      re =>{
        console.log(re);
        if(re==202){
          this.result=re;
        }
      }
    )
    // tslint:disable-next-line: no-var-keyword
    var itsSafe;
    if ( this.dateObject === undefined) {
      this.IsDateOK = false;
      itsSafe = 'No';
    }
    if (this.gr.value === undefined ) {
      this.IsHour = false;
      itsSafe = 'No';
    }
    // tslint:disable-next-line: curly
    if (itsSafe !== 'No'){
    // tslint:disable-next-line: quotemark
    document.getElementById("res").style.display= 'block';
    this.TheResultOfORder = "  شما در تاریخ  " + this.dateObject + "  و ساعت  " + this.gr.value + "وقت آرایشگاه دارید  ";
  }
  }
  GoToOrder(){
    let urladdrss = '/Order';
    this.r.navigateByUrl(urladdrss);
  }
  
  // getErrorMessage() {
  //   return this.DP.hasError('required') ? 'You must enter a value' :
  //       this.email.hasError('email') ? 'Not a valid email' :
  //           '';
  // }
}
export interface PeriodicElement {
  // name: string;
  // position: number;
  // weight: number;
  // symbol: string;
  // description: string;
  ID: number;
  Name: string;
  Score: number;
  Address: string;
  PhoneNumber: string;
}
