import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../Modules/material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { ListofBarberShopsComponent } from './listof-barber-shops/listof-barber-shops.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SetTimeForBarberShopComponent } from './listof-barber-shops/set-time-for-barber-shop/set-time-for-barber-shop.component';
import {DpDatePickerModule} from 'ng2-jalali-date-picker';
import { OrderedFavListComponent } from './ordered-fav-list/ordered-fav-list.component';
import { BaberPageComponent } from './baber-page/baber-page.component';

@NgModule({
  declarations: [
    AppComponent,
    ListofBarberShopsComponent,
    LoginComponent,
    SetTimeForBarberShopComponent,
    OrderedFavListComponent,
    BaberPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DpDatePickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
